create database ppl_buku;
use ppl_buku;

create table buku(
    id_buku int not null primary key auto_increment,
    nama_buku varchar(200) not null,
    penulis_buku varchar(200) not null,
    penerbit_buku varchar(200) 
);