<?php

class ModelBuku extends CI_Model {
    var $table = "buku";
    var $primaryKey = "id_buku";

    // untuk menampilkan data secara keseluruhan
    public function getAll()
    {
        return $this->db->get($this->table)->result();
    }
}