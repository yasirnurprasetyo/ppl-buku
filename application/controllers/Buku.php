<?php

class Buku extends CI_Controller {

    // ini buat load ModelBuku
    function __construct()
    {
        parent:: __construct();
        $this->load->model('ModelBuku');
    }

    // untuk menampilkan halaman list buku + datanya secara getAll()
    public function index()
    {
        $bukus = $this->ModelBuku->getAll();
        $data = array(
            "bukus" => $bukus
        );
        $this->load->view('buku/v_list_buku',$data);
    }
}